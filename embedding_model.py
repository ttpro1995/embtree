import gensim
import numpy as np


class EmbeddingModel:
    def __init__(self, pretrain_path):
        model = gensim.models.Word2Vec.load_word2vec_format(pretrain_path,
                                                            binary=True)  # C binary format
        self.model = model

    def embedding(self, word):
        model = self.model
        if word in model.vocab:
            return np.asarray(model[word], dtype='float32')
        else:
            return np.zeros(300, dtype= 'float32')


