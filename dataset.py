import pytreebank
import tree_util
import numpy as np
class Dataset:
    def __init__(self):
        self.embedding_model = None
        self.raw_train= None
        self.raw_test = None
        self.raw_dev = None

    def set_embedding_model(self, embedding_model):
        self.embedding_model = embedding_model

    def load_dataset(self, path):
        """

        :param path: path to trees folder
        :return:
        """
        trees = pytreebank.load_sst('trees')
        self.raw_train = trees["train"]
        self.raw_dev = trees["dev"]
        self.raw_test = trees["test"]

    def padding_sentence(self, sentence_token, length = 32):
        """
        Padding <EMPTY> before sentence to make fix length sentence
        :param sentence_token: "this is a string".split()
        :param length: default 32
        :return: list of word
        """
        words = sentence_token[:length]
        if (len(words)<length):
            words = ['<EMPTY>'] * (length - len(words)) + words
        return words

    def create_embedding_dataset(self, dataset_tag):
        trees = None
        if (dataset_tag == 'train'):
            trees = self.raw_train
        elif (dataset_tag == 'test'):
            trees = self.raw_test
        elif (dataset_tag == 'dev'):
            trees = self.raw_dev

        sentences, labels = tree_util.load_dataset(trees)
        embedding_model = self.embedding_model

        x = []

        for i in range(0, len(labels)):
            sentence = sentences[i]
            # embedding
            sentence_vec = []
            sentence_token = sentence.split()
            sentence_token = self.padding_sentence(sentence_token)

            for word in sentence_token:
                vec = embedding_model.embedding(word)  # some word will be ignore because no embedding
                sentence_vec.append(vec)
            sentence_vec = np.asarray(sentence_vec)
            x.append(sentence_vec)

        return (x, labels)

class Batch:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.n_sample = len(y)
        assert len(y) == len(x)
        self.left_idx = 0

    def get_batch(self, batch_size = 32):
        """
        get a batch of batch_size
        :param batch_size: default 32
        :return: x, y as list
        """
        right_idx = batch_size+self.left_idx
        x = self.x[self.left_idx:right_idx]
        y = self.y[self.left_idx:right_idx]
        self.left_idx = right_idx # update left idx for next batch
        return (x, y)

    def has_batch(self):
        """
        Check if we have any batch left
        :return:
        """
        if (self.left_idx < self.n_sample):
            return True



