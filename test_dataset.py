from dataset import Dataset
from embedding_model import EmbeddingModel
from dataset import Batch
import CONST


def test_dataset():
    embedding_model = EmbeddingModel(CONST.w2v_pretrained)
    dataset = Dataset()
    #result = dataset.padding_sentence("I am Pusheen the cat".split())
    #print result
    dataset.set_embedding_model(embedding_model)
    dataset.load_dataset(CONST.trees)
    x, y = dataset.create_embedding_dataset('dev')

    print ('x ',len(x))
    print ('y', len(y))
    print ('breakpoint')

def test_batch():
    b = Batch(range(1,20), range(101,120))
    while(b.has_batch()):
        x, y = b.get_batch(3)
        print (x ,' --- ',y)

def test_dataset_with_batch():
    embedding_model = EmbeddingModel(CONST.w2v_pretrained)
    dataset = Dataset()
    # result = dataset.padding_sentence("I am Pusheen the cat".split())
    # print result
    dataset.set_embedding_model(embedding_model)
    dataset.load_dataset(CONST.trees)
    x, y = dataset.create_embedding_dataset('dev')

    print ('x ', len(x))
    print ('y',  len(y))

    my_batch = Batch(x,y)
    x_batch, y_batch = my_batch.get_batch()
    print (len(x_batch))
    print (len(y_batch))
    print ('breakpoint')



if __name__ == "__main__":
    test_dataset_with_batch()