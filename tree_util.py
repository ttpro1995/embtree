import pytreebank

def load_dataset(ds, get_span = False):
    label = []
    sentences = []

    if (get_span):
        for tree in ds:
           for l, sent in tree.to_labeled_lines():
               label.append(l)
               sentences.append(sent)
    else:
        for tree in ds:
            l, sent = tree.to_labeled_lines()[0]
            label.append(l)
            sentences.append(sent)

    return (sentences, label)
